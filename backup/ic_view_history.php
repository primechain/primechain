<?php
    session_start();
    ob_start();
    include("check-login.php");
    include_once 'top-logged-in.php';
?>

<script type="text/javascript" src="js/indiacoin.js"></script>

<script type="text/javascript">

    function timer() {
        getTransactionsHistory('hWalletAddress', 'divoutput');
    }

    window.onload = function(){ timer(); setInterval(timer, 10000); };

</script>

<section role="main" class="content-body">

    <header class="page-header">
        <h2>Indiacoin</h2>
    
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="index.php">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Indiacoin - Transactions History</span></li>
            </ol>
    
            <a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a><!--  data-open="sidebar-right" -->
        </div>
    </header>

    <div class="row">
        <div class="col-md-12">
            <section class="panel panel-primary">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                        <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
                    </div>

                    <h2 class="panel-title">Recent Transactions</h2>
                </header>
                <div class="panel-body">
                    <!-- <form action="verify-login.php" method="post"> -->

                        <?php
                            $address = isset($_SESSION['address'])?$_SESSION['address']:"";
                            echo "<input type='hidden' id='hWalletAddress' value='".$address."' />";
                        ?>

                        <div id="divloader" class="row appear-animation fadeIn appear-animation-visible"></div>
                        
                        <div class="row">
                            <!-- <div class="col-md-1"></div> -->
                            <div id="divoutput" class="col-md-12 appear-animation fadeIn appear-animation-visible"></div>
                            <!-- <div class="col-md-1"></div> -->
                        </div>
                    <!-- </form> -->
                </div>
            </section>
        </div>

    </div>

</section>

<?php
    include_once 'bottom-logged-in.php';
?>