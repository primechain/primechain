<?php
	session_start();

	try {

		include_once "config.php";
		include_once "resources.php";
		include_once "MultichainClientTest.php";
		include_once "helperFunctions.php";
		include_once "dbhelper.php";

		$signerAddress = $_SESSION['address'];
		$signerID = $_SESSION['user_name'];

		if (isset($_GET['contractid'])) {
			$contractID = $_GET['contractid'];
		}
		else {
			throw new Exception("Invalid Contract ID", 1);
		}

		$dbHelper = new DBHelper(session_id(), $_SERVER);

		if (!$dbHelper->isValidContract($contractID)) {
			throw new Exception("Invalid Contract ID");
		}
		
		if (!$dbHelper->isAuthorizedToSignContract($_SESSION['address'], $contractID)) {
			throw new Exception("You are not authorized to sign this contract");
		}
		
		if ($dbHelper->hasSignedTheContract($_SESSION['address'], $contractID)) {
			throw new Exception("You have already signed this contract");
		}

		$contractDetails = $dbHelper->getContractDetails($contractID);

		$fileHash = $contractDetails[Literals::CONTRACT_DETAILS_FIELD_NAMES['FILE_HASH']];

		$mcTest = new MultichainClientTest();
		$mcTest->setUp(MultichainParams::HOST_NAME, MultichainParams::RPC_PORT, MultichainParams::RPC_USER, MultichainParams::RPC_PASSWORD);

		$signature = $mcTest->testSignMessage($signerAddress, $fileHash);
		$contractSignaturesStreamKey1 = $contractID;
		$contractSignaturesStreamKey2 = $contractID.Literals::STREAM_KEY_DELIMITER.$signerAddress;

		$contractSignatureArr = array(
			Literals::CONTRACT_SIGNATURES_FIELD_NAMES['SIGNER_ID'] => $signerID,
			Literals::CONTRACT_SIGNATURES_FIELD_NAMES['SIGNER_ADDRESS'] => $signerAddress,
			Literals::CONTRACT_SIGNATURES_FIELD_NAMES['SIGNATURE'] => $signature,
			Literals::CONTRACT_SIGNATURES_FIELD_NAMES['TIMESTAMP'] => date('d-M-Y H:i:s')
		);

		$contractSignatureJSON = json_encode($contractSignatureArr);
		$contractSignatureHex = bin2hex($contractSignatureJSON);		/// Hex encoding the metadata

		$mcTest->testPublishFrom($signerAddress, MultichainParams::CONTRACT_STREAMS['CONTRACT_SIGNATURES'], $contractSignaturesStreamKey1, $contractSignatureHex);
		$mcTest->testPublishFrom($signerAddress, MultichainParams::CONTRACT_STREAMS['CONTRACT_SIGNATURES'], $contractSignaturesStreamKey2, $contractSignatureHex);

		echo "<h4 style='color:green'>Signed Successfully</h4>";

	} 
	catch (Exception $e)
	{
		echo "<h3 style='color:red'>".$e->getMessage()."</h3>";
	}

?>