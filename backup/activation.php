<?php
	include_once "top.php";
?>
<br/><br/>
<div class="container">
	<div class="row mb-x1">
		<div class='col-md-12 center'>
			<?php

				try
				{
					require_once("helperFunctions.php");
					include_once "dbhelper.php";
					include_once "resources.php";
					include_once "config.php";

					class customException extends Exception {	}
					
					if(!isset($_GET['username']) || !isset($_GET['activationcode'])) { throw new customException("Invalid activation link!!!");  }
					$userName = htmlspecialchars($_GET['username'], ENT_QUOTES, 'UTF-8');
					$activationCode = htmlspecialchars($_GET['activationcode'], ENT_QUOTES, 'UTF-8');
					
					$dbHelper = new DBHelper(session_id(), $_SERVER);
					$accountStatusDetails = $dbHelper->getUserActivationDetails($userName);

					if(count($accountStatusDetails)>0)
					{
						if ($accountStatusDetails[Literals::USER_ACCOUNT_STATUS_FIELD_NAMES['ACTIVATION_CODE']]==$activationCode)
						{
							if ($dbHelper->activateUser($userName))
							{
								$address = $dbHelper->createUserAddress($userName);
								$permissions = "send".",";
								$permissions .= "receive";
								$dbHelper->grantPermissions($userName, $permissions);
								$permissions = MultichainParams::VAULT_STREAMS['DATA']."."."write";
								$dbHelper->grantPermissions($userName, $permissions);
								$permissions = MultichainParams::CONTRACT_STREAMS['CONTRACT_DETAILS']."."."write";
								$dbHelper->grantPermissions($userName, $permissions);
								$permissions = MultichainParams::CONTRACT_STREAMS['CONTRACT_FILES']."."."write";
								$dbHelper->grantPermissions($userName, $permissions);
								$permissions = MultichainParams::CONTRACT_STREAMS['CONTRACT_SIGNATURES']."."."write";
								$dbHelper->grantPermissions($userName, $permissions);
								$permissions = MultichainParams::CONTRACT_STREAMS['CONTRACT_INVITED_SIGNEES']."."."write";
								$dbHelper->grantPermissions($userName, $permissions);
								$dbHelper->sendInitIndiacoins($userName);
								echo "<p class='lead'><b><font color='green'>Your user account has been activated successfully.</font></b><br/></p>";
								echo "Click <a href='login.php' target='_new'>here</a> to login";
							}
							else
							{
								throw new customException("Activation Failed!!", 1);
							}
						}
						else
						{
							throw new customException("Invalid activation code!!", 1);							
						}
					}
					else
					{
						throw new customException("User details not found!!");
					}
				}
				catch (customException $ex)
				{
					echo "<p class='lead'><b><font color='red'>".$ex->getMessage()."</font></b></p>";
				}
				catch (Exception $ex)
				{
					echo "<p class='lead'><b><font color='red'>Activation Error!! Please try again after sometime or contact the site administrator.</font></b></p>".$ex->getMessage();
				}
				
			?>

		</div>
	</div>
</div>
<table>
	<tr height=100>
	</tr>
</table>

<?php
	include ("bottom.php");
?>