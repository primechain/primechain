<?php
	include_once 'resources.php';
?>
<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="Porto Admin - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.css" />

		<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="assets/stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="assets/vendor/modernizr/modernizr.js"></script>

	</head>
	<body>
		<!-- start: page -->
		<section class="body-sign">
			<div class="center-sign">
				<a href="/" class="logo pull-left">
					<img src="assets/images/logo.png" height="54" alt="Porto Admin" />
				</a>

				<div class="panel panel-sign">
					<div class="panel-title-sign mt-xl text-right">
						<h2 class="title text-uppercase text-weight-bold m-none"><i class="fa fa-user mr-xs"></i> Sign Up</h2>
					</div>

					<div class="panel-body">
						
						<!-- <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> -->
						<?php 
                            if(isset($_GET['msg']))
                            {
                            	echo "<div class='alert alert-danger'>";

                                if ($_GET['msg'] == '1') { echo "<strong>Error! Invalid email!!</strong>.<br/><br/>";}
                                if ($_GET['msg'] == '2') { echo "<strong>Error! Passwords do not match!!</strong>.<br/><br/>";}
                                if ($_GET['msg'] == '3') { echo "<strong>Invalid Username!!</strong>.<br/><br/>";}
                                if ($_GET['msg'] == '4') { echo "<strong> Invalid Password!!</strong>.<br/><br/>";}
                                if ($_GET['msg'] == '5') { echo "<strong>Username already exists</strong>.<br/><br/>";}
                                if ($_GET['msg'] == '6') { echo "<strong>Invalid name!!</strong>.<br/><br/>";}
                                if ($_GET['msg'] == '7') { echo "<strong>An user account for this email id already exists in our database!!</strong>.<br/><br/>";}
                                if ($_GET['msg'] == '8') { echo "<strong>Registration Error!! Please try again after sometime or contact the site administrator.</strong>.<br/><br/>";}

								echo "</div>";
                            }
                        ?>

						<form method="POST" action="verify-register.php">
							<div>
								<label for="name">Name <span class="required">*</span></label>
								<input id="name" name="name" type="text" class="form-control input-lg" required="true" />
							</div><br/>

							<div>
								<label for="username">User Name <span class="required">*</span></label>
								<input id="username" name="username" type="text" class="form-control input-lg" required="true" />
							</div><br/>

							<div>
								<label for="email">E-mail Address <span class="required">*</span></label>
								<input id="email" name="email" type="email" class="form-control input-lg" required="true" />
							</div><br/>


							<div>
								<label for="org">Organization <span class="required">*</span></label>
								<input id="org" name="org" type="text" class="form-control input-lg" required="true" />
							</div><br/>


							<div>
								<label for="country">Country <span class="required">*</span></label>
								<select id="country" name="country" data-plugin-selectTwo class="form-control populate">
									<?php
										foreach (Literals::COUNTRY_DESC as $code => $desc) {
											echo "<option value='".$code."' ".(($code=="IN")?"selected":"").">".$desc."</option>";
										}
									?>
									<option></option>
								</select>
							</div><br/>

							<!-- <div class="form-group mb-none">
								<div class="row">
									<div class="col-sm-6 mb-lg">
										<label>Password</label>
										<input name="pwd" type="password" class="form-control input-lg" />
									</div>
									<div class="col-sm-6 mb-lg">
										<label>Password Confirmation</label>
										<input name="pwd_confirm" type="password" class="form-control input-lg" />
									</div>
								</div>
							</div> -->

							<div class="row">
								<div class="col-sm-8">
									<div class="checkbox-custom checkbox-default">
										<input id="AgreeTerms" name="agreeterms" type="checkbox" required="true" />
										<label for="AgreeTerms">I agree with <a href="#">terms of use</a> <span class="required">*</span></label>
									</div>
								</div>
								<div class="col-sm-4 text-right">
									<button type="submit" class="btn btn-primary hidden-xs">Sign Up</button>
									<!-- <button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Sign Up</button> -->
								</div>
							</div><br/>

							<p class="text-center">Already have an account? <a href="login.php">Sign In!</a></p>

						</form>
					</div>
				</div>

				<p class="text-center text-muted mt-md mb-md">&copy; Copyright 2017. All Rights Reserved.</p>
			</div>
		</section>
		<!-- end: page -->

		<!-- Vendor -->
		<script src="assets/vendor/jquery/jquery.js"></script>
		<script src="assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="assets/vendor/magnific-popup/jquery.magnific-popup.js"></script>
		<script src="assets/vendor/jquery-placeholder/jquery-placeholder.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="assets/javascripts/theme.init.js"></script>

	</body>
</html>