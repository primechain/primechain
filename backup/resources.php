<?php

	/**
	* 
	*/
	class Literals
	{

		/**
		 *  Array of country codes (ISO 3166-1 alpha-2) and corresponding names. 
		*/
		const COUNTRY_DESC = array(
			    'AF' => 'Afghanistan',
			    'AX' => 'Aland Islands',
			    'AL' => 'Albania',
			    'DZ' => 'Algeria',
			    'AS' => 'American Samoa',
			    'AD' => 'Andorra',
			    'AO' => 'Angola',
			    'AI' => 'Anguilla',
			    'AQ' => 'Antarctica',
			    'AG' => 'Antigua and Barbuda',
			    'AR' => 'Argentina',
			    'AM' => 'Armenia',
			    'AW' => 'Aruba',
			    'AU' => 'Australia',
			    'AT' => 'Austria',
			    'AZ' => 'Azerbaijan',
			    'BS' => 'Bahamas',
			    'BH' => 'Bahrain',
			    'BD' => 'Bangladesh',
			    'BB' => 'Barbados',
			    'BY' => 'Belarus',
			    'BE' => 'Belgium',
			    'BZ' => 'Belize',
			    'BJ' => 'Benin',
			    'BM' => 'Bermuda',
			    'BT' => 'Bhutan',
			    'BO' => 'Bolivia',
			    'BQ' => 'Bonaire, Saint Eustatius and Saba',
			    'BA' => 'Bosnia and Herzegovina',
			    'BW' => 'Botswana',
			    'BV' => 'Bouvet Island',
			    'BR' => 'Brazil',
			    'IO' => 'British Indian Ocean Territory',
			    'VG' => 'British Virgin Islands',
			    'BN' => 'Brunei',
			    'BG' => 'Bulgaria',
			    'BF' => 'Burkina Faso',
			    'BI' => 'Burundi',
			    'KH' => 'Cambodia',
			    'CM' => 'Cameroon',
			    'CA' => 'Canada',
			    'CV' => 'Cape Verde',
			    'KY' => 'Cayman Islands',
			    'CF' => 'Central African Republic',
			    'TD' => 'Chad',
			    'CL' => 'Chile',
			    'CN' => 'China',
			    'CX' => 'Christmas Island',
			    'CC' => 'Cocos Islands',
			    'CO' => 'Colombia',
			    'KM' => 'Comoros',
			    'CK' => 'Cook Islands',
			    'CR' => 'Costa Rica',
			    'HR' => 'Croatia',
			    'CU' => 'Cuba',
			    'CW' => 'Curacao',
			    'CY' => 'Cyprus',
			    'CZ' => 'Czech Republic',
			    'CD' => 'Democratic Republic of the Congo',
			    'DK' => 'Denmark',
			    'DJ' => 'Djibouti',
			    'DM' => 'Dominica',
			    'DO' => 'Dominican Republic',
			    'TL' => 'East Timor',
			    'EC' => 'Ecuador',
			    'EG' => 'Egypt',
			    'SV' => 'El Salvador',
			    'GQ' => 'Equatorial Guinea',
			    'ER' => 'Eritrea',
			    'EE' => 'Estonia',
			    'ET' => 'Ethiopia',
			    'FK' => 'Falkland Islands',
			    'FO' => 'Faroe Islands',
			    'FJ' => 'Fiji',
			    'FI' => 'Finland',
			    'FR' => 'France',
			    'GF' => 'French Guiana',
			    'PF' => 'French Polynesia',
			    'TF' => 'French Southern Territories',
			    'GA' => 'Gabon',
			    'GM' => 'Gambia',
			    'GE' => 'Georgia',
			    'DE' => 'Germany',
			    'GH' => 'Ghana',
			    'GI' => 'Gibraltar',
			    'GR' => 'Greece',
			    'GL' => 'Greenland',
			    'GD' => 'Grenada',
			    'GP' => 'Guadeloupe',
			    'GU' => 'Guam',
			    'GT' => 'Guatemala',
			    'GG' => 'Guernsey',
			    'GN' => 'Guinea',
			    'GW' => 'Guinea-Bissau',
			    'GY' => 'Guyana',
			    'HT' => 'Haiti',
			    'HM' => 'Heard Island and McDonald Islands',
			    'HN' => 'Honduras',
			    'HK' => 'Hong Kong',
			    'HU' => 'Hungary',
			    'IS' => 'Iceland',
			    'IN' => 'India',
			    'ID' => 'Indonesia',
			    'IR' => 'Iran',
			    'IQ' => 'Iraq',
			    'IE' => 'Ireland',
			    'IM' => 'Isle of Man',
			    'IL' => 'Israel',
			    'IT' => 'Italy',
			    'CI' => 'Ivory Coast',
			    'JM' => 'Jamaica',
			    'JP' => 'Japan',
			    'JE' => 'Jersey',
			    'JO' => 'Jordan',
			    'KZ' => 'Kazakhstan',
			    'KE' => 'Kenya',
			    'KI' => 'Kiribati',
			    'XK' => 'Kosovo',
			    'KW' => 'Kuwait',
			    'KG' => 'Kyrgyzstan',
			    'LA' => 'Laos',
			    'LV' => 'Latvia',
			    'LB' => 'Lebanon',
			    'LS' => 'Lesotho',
			    'LR' => 'Liberia',
			    'LY' => 'Libya',
			    'LI' => 'Liechtenstein',
			    'LT' => 'Lithuania',
			    'LU' => 'Luxembourg',
			    'MO' => 'Macao',
			    'MK' => 'Macedonia',
			    'MG' => 'Madagascar',
			    'MW' => 'Malawi',
			    'MY' => 'Malaysia',
			    'MV' => 'Maldives',
			    'ML' => 'Mali',
			    'MT' => 'Malta',
			    'MH' => 'Marshall Islands',
			    'MQ' => 'Martinique',
			    'MR' => 'Mauritania',
			    'MU' => 'Mauritius',
			    'YT' => 'Mayotte',
			    'MX' => 'Mexico',
			    'FM' => 'Micronesia',
			    'MD' => 'Moldova',
			    'MC' => 'Monaco',
			    'MN' => 'Mongolia',
			    'ME' => 'Montenegro',
			    'MS' => 'Montserrat',
			    'MA' => 'Morocco',
			    'MZ' => 'Mozambique',
			    'MM' => 'Myanmar',
			    'NA' => 'Namibia',
			    'NR' => 'Nauru',
			    'NP' => 'Nepal',
			    'NL' => 'Netherlands',
			    'NC' => 'New Caledonia',
			    'NZ' => 'New Zealand',
			    'NI' => 'Nicaragua',
			    'NE' => 'Niger',
			    'NG' => 'Nigeria',
			    'NU' => 'Niue',
			    'NF' => 'Norfolk Island',
			    'KP' => 'North Korea',
			    'MP' => 'Northern Mariana Islands',
			    'NO' => 'Norway',
			    'OM' => 'Oman',
			    'PK' => 'Pakistan',
			    'PW' => 'Palau',
			    'PS' => 'Palestinian Territory',
			    'PA' => 'Panama',
			    'PG' => 'Papua New Guinea',
			    'PY' => 'Paraguay',
			    'PE' => 'Peru',
			    'PH' => 'Philippines',
			    'PN' => 'Pitcairn',
			    'PL' => 'Poland',
			    'PT' => 'Portugal',
			    'PR' => 'Puerto Rico',
			    'QA' => 'Qatar',
			    'CG' => 'Republic of the Congo',
			    'RE' => 'Reunion',
			    'RO' => 'Romania',
			    'RU' => 'Russia',
			    'RW' => 'Rwanda',
			    'BL' => 'Saint Barthelemy',
			    'SH' => 'Saint Helena',
			    'KN' => 'Saint Kitts and Nevis',
			    'LC' => 'Saint Lucia',
			    'MF' => 'Saint Martin',
			    'PM' => 'Saint Pierre and Miquelon',
			    'VC' => 'Saint Vincent and the Grenadines',
			    'WS' => 'Samoa',
			    'SM' => 'San Marino',
			    'ST' => 'Sao Tome and Principe',
			    'SA' => 'Saudi Arabia',
			    'SN' => 'Senegal',
			    'RS' => 'Serbia',
			    'SC' => 'Seychelles',
			    'SL' => 'Sierra Leone',
			    'SG' => 'Singapore',
			    'SX' => 'Sint Maarten',
			    'SK' => 'Slovakia',
			    'SI' => 'Slovenia',
			    'SB' => 'Solomon Islands',
			    'SO' => 'Somalia',
			    'ZA' => 'South Africa',
			    'GS' => 'South Georgia and the South Sandwich Islands',
			    'KR' => 'South Korea',
			    'SS' => 'South Sudan',
			    'ES' => 'Spain',
			    'LK' => 'Sri Lanka',
			    'SD' => 'Sudan',
			    'SR' => 'Suriname',
			    'SJ' => 'Svalbard and Jan Mayen',
			    'SZ' => 'Swaziland',
			    'SE' => 'Sweden',
			    'CH' => 'Switzerland',
			    'SY' => 'Syria',
			    'TW' => 'Taiwan',
			    'TJ' => 'Tajikistan',
			    'TZ' => 'Tanzania',
			    'TH' => 'Thailand',
			    'TG' => 'Togo',
			    'TK' => 'Tokelau',
			    'TO' => 'Tonga',
			    'TT' => 'Trinidad and Tobago',
			    'TN' => 'Tunisia',
			    'TR' => 'Turkey',
			    'TM' => 'Turkmenistan',
			    'TC' => 'Turks and Caicos Islands',
			    'TV' => 'Tuvalu',
			    'VI' => 'U.S. Virgin Islands',
			    'UG' => 'Uganda',
			    'UA' => 'Ukraine',
			    'AE' => 'United Arab Emirates',
			    'GB' => 'United Kingdom',
			    'US' => 'United States',
			    'UM' => 'United States Minor Outlying Islands',
			    'UY' => 'Uruguay',
			    'UZ' => 'Uzbekistan',
			    'VU' => 'Vanuatu',
			    'VA' => 'Vatican',
			    'VE' => 'Venezuela',
			    'VN' => 'Vietnam',
			    'WF' => 'Wallis and Futuna',
			    'EH' => 'Western Sahara',
			    'YE' => 'Yemen',
			    'ZM' => 'Zambia',
			    'ZW' => 'Zimbabwe',
			);

		const STREAM_KEY_DELIMITER = "~";

		const USER_CREDENTIALS_FIELD_NAMES = array(
				"USER_NAME" => "user_name",
				"PASSWORD_HASH" => "pass_hash"
			);

		const USER_DETAILS_FIELD_NAMES = array(
				"USER_NAME" => "user_name",
				"NAME" => "name",
				"ORGANIZATION" => "org",
				"EMAIL" => "email",
				"COUNTRY" => "country"
			);

		const USER_ACCOUNT_STATUS_FIELD_NAMES = array(
				"USER_NAME" => "user_name",
				"ACCOUNT_STATUS" => "acc_status",
				"ACTIVATION_CODE" => "act_code"
			);

		const USER_SESSION_FIELD_NAMES = array(
				"USER_NAME" => "user_name",
				"SESSION_ID" => "sess_id",
				"SESSION_IP" => "sess_ip"
			);

		const USER_ADDRESS_FIELD_NAMES = array(
				"USER_NAME" => "user_name",
				"ADDRESS" => "addr"
			);

		const USER_AUTH_CODE_FIELD_NAMES = array(
				"USER_NAME" => "user_name",
				"AUTH_CODE" => "auth",
				"AUTH_TIME" => "timestamp"
			);

		const LOGS_FIELD_NAMES = array(
				"USER_NAME" => "user_name",
				"ACTIVITY_TYPE" => "act_type",
				"TRANSACTION_ID" => "txid",
				"IP_ADDRESS" => "IP",
				"TIMESTAMP" => "time"
			);

		const ACTIVITY_TYPE_CODES = array(
				"CREATE_USER_CREDS" => "create_usr_creds",
				"CREATE_USER_DETAILS" => "create_usr_details",
				"CREATE_USER_ACTIVATION_DETAILS" => "create_usr_act_details",
				"ACTIVATE_USER" => "activate_usr",
				"ADDRESS_GENERATE" => "addr_gen",
				"GRANT" => "grant",
				"AUTH_CODE_GENERATED" => "auth_code_gen",
				"SESSION_CREATE" => "sess_create",
				"INIT_INDIACOIN" => "init_indiacoin",
				"INVITE_SIGNEE" => "invite_signee"
			);

		const CONTRACT_DETAILS_FIELD_NAMES = array(
				"CONTRACT_ID" => "contract_id",
				"TITLE" => "title",
				"DESCRIPTION" => "desc",
				"DATE_OF_UPLOAD" => "dou",
				"FILE_HASH" => "hash"
			);

		const CONTRACT_DETAILS_FIELD_DESC = array(
				"contract_id" => "Contract ID",
				"title" => "Title",
				"desc" => "Description",
				"dou" => "Date Of Creation",
				"hash" => "File Hash"
			);

		const CONTRACT_INVITED_SIGNEES_FIELD_NAMES = array(
				"INVITEE_ID" => "invitee_id",
				"INVITEE_ADDRESS" => "invitee_addr",
				"CONTRACT_ID" => "contract_id"
			);

		const CONTRACT_INVITED_SIGNEES_FIELD_DESC = array(
				"invitee_id" => "Invitee ID",
				"invitee_addr" => "Invitee's address",
				"contract_id" => "Contract ID"
			);

		const CONTRACT_SIGNATURES_FIELD_NAMES = array(
				"SIGNER_ID" => "signer_id",
				"SIGNER_ADDRESS" => "signer_addr",
				"SIGNATURE" => "sign",
				"TIMESTAMP" => "timestamp"
			);

		const CONTRACT_SIGNATURES_FIELD_DESC = array(
				"signer_id" => "Signer ID",
				"signer_addr" => "Signer's address",
				"sign" => "Signature",
				"timestamp" => "Timestamp"
			);

		const VAULT_FIELDS_CODES = array(
			"date_of_upload" => "dou",
			"description" => "desc",
			"file_hex" => "file_hex"
			);

		const VAULT_FIELDS_DESC = array(
			"dou" => "Date of Upload",
			"desc" => "Description",
			"file_hex" => "File data Hexadecimal"
			);

		const MULTICHAIN_COMMANDS_CODES = array(
			"STA"=>"sendtoaddr", 
			"SWM"=>"sendwithmetadata", 
			"NCB"=>"nativecurrencybalance", 
			"MARK_READ"=>"mark_read", 
			"FETCH_MSGS"=>"fetchmessages", 
			"FETCH_MSGS_COUNT"=>"fetchmessagescount", 
			"GET_TX_DETAILS"=>"gettransactiondetails", 
			"GET_BLOCK_DETAILS"=>"getblockdetails", 
			"GET_TRANSACTIONS_HISTORY"=>"gettransactionshistory"
			);
	}

?>