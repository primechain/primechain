<?php
	ob_start();
	include_once ("top.php");
?>

<section role="main" class="content-body">

	<header class="page-header">
		<h2>Registration</h2>
	
		<div class="right-wrapper pull-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.php">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><span>Registration Successful</span></li>
			</ol>
	
			<a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a> <!-- data-open="sidebar-right" -->
		</div>
	</header>

	<div class="row">
		<div class="col-md-10">
			<?php
				echo "<p class='lead'><b><font color='blue'>Account registration successful. Please click the activation link sent to your registered email id to activate your account.</font></b></p>";
			?>
		</div>
	</div>
</section>

<?php
	include_once 'bottom.php';
?>