<?php
	session_start();
	require_once('MultichainClientTest.php');
	require_once('resources.php');
	require_once('helperFunctions.php');
    require_once('config.php');

	try
	{
		if (isset($_GET['txid']))
		{
			
			$txId = $_GET['txid'];
			$uploader_address = $_GET['publisher'];

			$mcTest = new MultichainClientTest();
			$mcTest->setUp(MultichainParams::HOST_NAME, MultichainParams::RPC_PORT, MultichainParams::RPC_USER, MultichainParams::RPC_PASSWORD);

			if (isset($_GET['v_n']))
			{
				$vOut_n = intval($_GET['v_n']);
				$dataHex = $mcTest->testGetTxOutData($txId, $vOut_n);
			}
			else
			{
				$transaction = $mcTest->testGetAddressTransaction($uploader_address, $txId);
				$dataHex = $transaction['data'][0];
			}
			
			$fileContentHex = $dataHex;

			$fileSignature = strtoupper(substr($fileContentHex, 0, 20));

			$fileDataType = getFileDataType($fileSignature);

			$downloadURL = "data:".$fileDataType.";base64,".base64_encode(pack('H*', $fileContentHex));
			echo "<script>window.onload=function(){window.open('".$downloadURL."', '_self')}</script>";
			
		}
		else
		{
			throw new Exception("No Transaction ID found.");
		}
	}
	catch(Exception $e)
	{
		echo "<h3 style='color:red'>".$e->getMessage()."</h3>";
	}

?>