<?php
	ob_start();
	session_start();
	include_once 'top-logged-in.php';
?>

<section role="main" class="content-body">

	<header class="page-header">
		<h2>Dashboard</h2>
	
		<div class="right-wrapper pull-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.php">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><span>Dashboard</span></li>
			</ol>
			
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
		</div>
	</header>

	<div class="row">
        <div class="col-md-10">

        	<!-- 
				The body of the page goes here
        	 -->
        	 
        </div>
    </div>

</section>

<?php
	include_once 'bottom-logged-in.php';
	ob_end_flush();
?>