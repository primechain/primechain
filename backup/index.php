<?php
	ob_start();
	include_once 'top.php';
?>

<?php
	header("location:login.php");
?>
<section role="main" class="content-body">

	<header class="page-header">
		<h2>Dashboard</h2>
	
		<div class="right-wrapper pull-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.php">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><span>Dashboard</span></li>
			</ol>
	
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
		</div>
	</header>


</section>

<?php
	include_once 'bottom.php';
	ob_end_flush();
?>