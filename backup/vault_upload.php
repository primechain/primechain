<?php
    session_start();
    include_once 'top-logged-in.php';
?>

<script type="text/javascript" src="js/vault.js"></script>

<section role="main" class="content-body">

    <header class="page-header">
        <h2>PrimeVault</h2>
    
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="index.php">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>PrimeVault - Upload</span></li>
            </ol>
    
            <a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a><!--  data-open="sidebar-right" -->
        </div>
    </header>

    <div class="row">
        <div class="col-md-10">
            <section class="panel panel-primary">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                        <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
                    </div>

                    <h2 class="panel-title">Upload document</h2>
                </header>
                <div class="panel-body">
                    <!-- <form action="verify-login.php" method="post"> -->

                        <div class="form-group appear-animation fadeIn appear-animation-visible">
                            <label class="col-sm-3 control-label"><strong>Date of Upload </strong><span class="required">*</span></label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <!-- <input type="text" data-plugin-datepicker class="form-control"> -->
                                    <input class="form-control" type="datetime-local" id="dou" name="dou" value="<?php date_default_timezone_set('Asia/Kolkata'); echo str_replace("/", "T", date('Y-m-d/H:i:s')); ?>" required /> 
                                </div>
                            </div>
                        </div>
                        <br/>

                        <div class="form-group appear-animation fadeIn appear-animation-visible">
                            <label class="col-sm-3 control-label"><strong>File Upload </strong> <span class="required">*</span></label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="input-append">
                                            <div class="uneditable-input">
                                                <i class="fa fa-file fileupload-exists"></i>
                                                <span class="fileupload-preview"></span>
                                            </div>
                                            <span class="btn btn-default btn-file">
                                                <span class="fileupload-exists">&nbsp;</span>
                                                <span class="fileupload-new">&nbsp;</span>
                                                <input id="file" type="file" name="filename" accept="image/*,application/pdf" />
                                            </span>
                                            <a href="#" class="btn btn-warning fileupload-exists" data-dismiss="fileupload">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br/>

                        <div class="form-group appear-animation fadeIn appear-animation-visible">
                            <label class="col-sm-3 control-label"><strong>Description </strong></label>
                            <div class="col-sm-9">
                                <textarea id="desc" name="desc" rows="5" class="form-control" placeholder="Description" required></textarea>
                            </div>
                        </div>
                        <br/>

                        <div class="progress light m-md" style="width: 50%" hidden="true">
                            <label id="progressLabel" for="progress"></label>
                            <div id="progress" class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 50px;">
                                Upload progress
                            </div>
                        </div>

                        <div class="row appear-animation fadeIn appear-animation-visible">
                            <div class="col-sm-12 text-left">
                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary" onclick="uploadFile(dou, file, desc, output); return false;">Upload</button>
                                <!-- <button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Sign In</button> -->
                            </div>
                        </div><br/>

                        <div class="row form-group appear-animation fadeIn appear-animation-visible">
                            <div class="col-sm-12 text-left">
                                <div id="output" class="appear-animation fadeIn appear-animation-visible">

                                </div>
                            </div>
                        </div>



                    <!-- </form> -->
                </div>
            </section>
        </div>

    </div>

</section>

<?php
    include_once 'bottom-logged-in.php';
?>