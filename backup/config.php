<?php

	/**
	 *  INDIACOIN PARAMETERS
	 */
	class IndiacoinParams
	{
		const INIT_QTY = 1000;
		const ISSUE_TXID = "42f2b34eb5843a07606d8be155b72dbafaddaaea7cd070d0245441b83db100ce";
		const ASSET_REF = "16696-301-62018";
		const ASSET_NAME = "indiacoin";
	}

	/**
	 *  Explorer Parameters
	 */
	class ExplorerParams
	{

		const HOST_NAME = "159.203.168.224";
		const PORT = "8080";
		const CHAIN_NAME = "Primechain";
		const TX_URL_PREFIX = "http://".ExplorerParams::HOST_NAME.":".ExplorerParams::PORT."/".ExplorerParams::CHAIN_NAME."/tx/";
		const BLOCK_URL_PREFIX = "http://".ExplorerParams::HOST_NAME.":".ExplorerParams::PORT."/".ExplorerParams::CHAIN_NAME."/block/";
		const ADDRESS_URL_PREFIX = "http://".ExplorerParams::HOST_NAME.":".ExplorerParams::PORT."/".ExplorerParams::CHAIN_NAME."/address/";
	}

	/**
	* 
	*/
	class MultichainParams
	{
		const HOST_NAME = "localhost";
		const RPC_PORT = "6820";
		const RPC_USER = "multichainrpc";
		const RPC_PASSWORD = "CYw4pp91TF3Tm8pNhgbu4hFgZiRQNccbCnGhHXRuhfee";
		const MANAGER_ADDRESS = "1Mdqxe9WaiQLKnCorD7Ncu2VxZq9Cefn9qPKR4";
		
		const USER_STREAMS = array(
				"USERS_CREDENTIALS" => "users_credentials",
				"USERS_DETAILS" => "users_details",
				"USERS_ADDRESSES" => "users_addresses",
				"USERS_ACCOUNTS_STATUSES" => "users_accounts_statuses",
				"USERS_SESSION" => "users_session",
				"USERS_LOGS" => "users_logs",
				"USERS_NODES" => "users_nodes",
				"USERS_AUTH_CODES" => "users_auth_codes"
			);

		const IC_STREAMS = array(
				"USERS_CREDENTIALS" => "users_credentials",
				"USERS_DETAILS" => "users_details"
			);

		const CONTRACT_STREAMS = array(
				"CONTRACT_DETAILS" => "contract_details",
				"CONTRACT_FILES" => "contract_files",
				"CONTRACT_SIGNATURES" => "contract_signatures",
				"CONTRACT_INVITED_SIGNEES" => "contract_invited_signees"
			);

		const VAULT_STREAMS = array(
				"DATA" => "vault"
			);
	}


	/**
	* 
	*/
	class WebServerParams
	{		
		const HOST_NAME = "159.203.168.224";				// IP address or Host Name of the Web Server 
		const PRIMECHAIN_ROOT_DIR = "primechain";	// Root directory of Primechain
	}

?>