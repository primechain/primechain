<?php
	session_start();
	include_once "config.php";
	include_once "resources.php";
	include_once "MultichainClientTest.php";
	include_once "helperFunctions.php";

	try
	{
		if(isset($_POST["dou"]))
		{
			$dateOfUpload = DateTime::createFromFormat('Y-m-d H:i:s', str_replace("T", " ", $_POST["dou"]));
		}
		else
		{
			$dateOfUpload = DateTime::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'));
		}

		$mcTest = new MultichainClientTest();
		$mcTest->setUp(MultichainParams::HOST_NAME, MultichainParams::RPC_PORT, MultichainParams::RPC_USER, MultichainParams::RPC_PASSWORD);
		
		$uploader_address = $_SESSION['address'];
		$uploader_ID = $_SESSION['user_name'];

		$dateOfUploadStr = $dateOfUpload->format('d-M-Y H:i:s');
		$title = isset($_POST["title"])?$_POST["title"]:"";
		$desc = isset($_POST["desc"])?$_POST["desc"]:"";
		$file = $_FILES['filename'];
		$target_file = $_FILES['filename']['tmp_name'];

	/// Reading file contents
		$handle = fopen($target_file, "rb");
		$fileContentHex = bin2hex(fread($handle, filesize($target_file)));
		fclose($handle);
		
		$fileHash = hash_file('sha256', $target_file);

		$signature = $mcTest->testSignMessage($uploader_address, $fileHash);
		$contractID = generateGUID();
		$contractSignaturesStreamKey1 = $contractID;
		$contractSignaturesStreamKey2 = $contractID.Literals::STREAM_KEY_DELIMITER.$uploader_address;

		$contractDetailsArr = array(
			Literals::CONTRACT_DETAILS_FIELD_NAMES['TITLE'] => $title,
			Literals::CONTRACT_DETAILS_FIELD_NAMES['DATE_OF_UPLOAD'] => $dateOfUploadStr,
			Literals::CONTRACT_DETAILS_FIELD_NAMES['DESCRIPTION'] => $desc,
			Literals::CONTRACT_DETAILS_FIELD_NAMES['FILE_HASH'] => $fileHash
		);

		$contractSignatureArr = array(
			Literals::CONTRACT_SIGNATURES_FIELD_NAMES['SIGNER_ID'] => $uploader_ID,
			Literals::CONTRACT_SIGNATURES_FIELD_NAMES['SIGNER_ADDRESS'] => $uploader_address,
			Literals::CONTRACT_SIGNATURES_FIELD_NAMES['SIGNATURE'] => $signature,
			Literals::CONTRACT_SIGNATURES_FIELD_NAMES['TIMESTAMP'] => $dateOfUploadStr
		);

		$contractDetailsJSON = json_encode($contractDetailsArr);
		$contractDetailsHex = bin2hex($contractDetailsJSON);		/// Hex encoding the metadata

		$contractSignatureJSON = json_encode($contractSignatureArr);
		$contractSignatureHex = bin2hex($contractSignatureJSON);		/// Hex encoding the metadata

		unlink($target_file);
		$mcTest->testPublishFrom($uploader_address, MultichainParams::CONTRACT_STREAMS['CONTRACT_DETAILS'], $contractID, $contractDetailsHex);	/// Publisher address and stream name to be modified
		$mcTest->testPublishFrom($uploader_address, MultichainParams::CONTRACT_STREAMS['CONTRACT_FILES'], $contractID, $fileContentHex);
		$mcTest->testPublishFrom($uploader_address, MultichainParams::CONTRACT_STREAMS['CONTRACT_SIGNATURES'], $contractSignaturesStreamKey1, $contractSignatureHex);
		$mcTest->testPublishFrom($uploader_address, MultichainParams::CONTRACT_STREAMS['CONTRACT_SIGNATURES'], $contractSignaturesStreamKey2, $contractSignatureHex);

		echo "<b><font color='green'>Transaction Successful.<br/>"."Your Contract ID is </font></b>"."<a target='_new' href='contract_upload_details.php?contractid=".$contractID."'>".$contractID."</a>";
	}
	catch (exception $ex)
	{
		echo "<font color='red'><b>".$ex->getMessage()."</b></font>";
	}

?>