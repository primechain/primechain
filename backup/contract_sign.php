<?php
    session_start();
	include_once 'top-logged-in.php';
    //include_once 'top.php';
?>

<script type="text/javascript" src="js/contract.js"></script>

<section role="main" class="content-body">

    <header class="page-header">
        <h2>PrimeContract</h2>
    
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="index.php">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>PrimeContract</span></li>
                <li><span>Sign</span></li>
            </ol>
    
            <a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a><!--  data-open="sidebar-right" -->
        </div>
    </header>

    <div class="row">
        <div class="col-md-10">
            <section class="panel panel-primary">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                        <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
                    </div>

                    <h2 class="panel-title">Sign Contract</h2>
                </header>
                <div class="panel-body">
                    <!-- <form action="verify-login.php" method="post"> -->

                    	<!-- <div class="form-group appear-animation fadeIn appear-animation-visible">
                            <label class="col-sm-3 control-label"><strong>Contract ID </strong></label>
                            <div class="col-sm-9">
                                <input id="contractid" name="contractid" class="form-control" placeholder="Contract ID" required="true">
                            </div>
                        </div>
                        <br/>

                        <div class="row appear-animation fadeIn appear-animation-visible">
                            <div class="col-sm-12 text-left">
                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary" onclick="verifyAndSignContract(this, contractid, output); contract_id.value = contractid.value; return false;">Verify and Sign</button>
                            </div>
                        </div><br/>

                        <div class="progress light m-md" style="width: 100px" hidden="true">
                            <label id="progressLabel" for="progress"></label>
                            <div id="progress" class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" hidden="true" style="width: 100px;">
                                
                            </div>
                        </div>

                        <div class="row appear-animation fadeIn appear-animation-visible">
                            <div id="output" class="col-md-11">
                            </div>
                        </div>

                        <div class="row appear-animation fadeIn appear-animation-visible">
                            <div id="sign_confirm" hidden="true" class="col-md-11">
                                <div>
                                    <input type="hidden" id="contract_id">
                                    <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary" onclick="signContract(this, contract_id, output_sign); return false;">Sign</button>
                                </div>
                                <div id="output_sign">
                                
                                </div>
                            </div>
                        </div><br/> -->

                        <div class="row appear-animation fadeIn appear-animation-visible">
                            <div class="col-md-11">

                                <div id="output_sign1">
                                     <?php
                                        if (isset($_GET['msg']))
                                        {
                                            if ($_GET['msg'] == 1) { echo "<h3 style='color:green'>Signing successful!</h3>"; }
                                        }
                                     ?>
                                </div>

                                <?php
                                    include_once 'dbhelper.php';
                                    include_once 'resources.php';

                                    $dbHelper = new DBHelper(session_id(), $_SERVER);
                                    $pendingContractsDetails = $dbHelper->getContractsPendingSignature($_SESSION['address']);

                                    if (count($pendingContractsDetails)>0)
                                    {

                                        echo "<h3 style='color:#0066cc'>Contracts pending signature:</h3>";

                                        echo "<p class='appear-animation fadeInDown appear-animation-visible'><table class='table table-bordered table-hover'>";
                                        echo "<tr><th>".Literals::CONTRACT_DETAILS_FIELD_DESC['title']."</th><th>".Literals::CONTRACT_DETAILS_FIELD_DESC['desc']."</th><th>".Literals::CONTRACT_DETAILS_FIELD_DESC['dou']."</th><th></th><th></th></tr>";

                                        foreach ($pendingContractsDetails as $contractDetails)
                                        {
                                            echo "<tr>";

                                            foreach ($contractDetails as $key => $value)
                                            {
                                                if ($key != Literals::CONTRACT_DETAILS_FIELD_NAMES['FILE_HASH'] && $key != Literals::CONTRACT_DETAILS_FIELD_NAMES['CONTRACT_ID']) {
                                                    echo "<td>".$value."</td>";
                                                }
                                            }

                                            echo "<td><a class='mb-xs mt-xs mr-xs btn btn-primary' target='_new' href='contract_upload_details.php?contractid=".$contractDetails[Literals::CONTRACT_DETAILS_FIELD_NAMES['CONTRACT_ID']]."'>View</a></td>";

                                            echo "<td><button class='mb-xs mt-xs mr-xs btn btn-primary' onclick='contract_id.value=\"".$contractDetails[Literals::CONTRACT_DETAILS_FIELD_NAMES['CONTRACT_ID']]."\";signContract(this, contract_id, output_sign1); return false;'>Sign</button></td>";

                                            echo "</tr>";
                                        }

                                        echo "</table></p>";
                                    }
                                    else
                                    {
                                        echo "<h3 style='color:green'><b>No contracts pending signature</b></h3>";
                                    }
                                ?>
                            </div>
                        </div>

                    <!-- </form> -->
                </div>
            </section>
        </div>

    </div>

</section>

<?php
    include_once 'bottom-logged-in.php';
?>