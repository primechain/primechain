<?php
	session_start();
	ob_start();
	include_once ("top.php");
	require_once("config.php");
	require_once("helperFunctions.php");
	require_once("dbhelper.php");
	include_once ("resources.php");

	try
	{
		if(isset($_POST['username']) && isset($_POST['password']))
		{
			$userName = $_POST['username'];
			$password = $_POST['password'];
			$ipAddr = $_SERVER['REMOTE_ADDR'];

			/*if(!validateUsername($userName))
			{
				header("location:login.php?msg=1");
			}*/

			// if(!validatePassword($password))
			// {
			// 	header("location:login.php?msg=1");
			// }

			$dbHelper = new DBHelper(session_id(), $_SERVER);

			$userCredentials = $dbHelper->getUserCredentials($userName);
			
			if(password_verify($password, $userCredentials[Literals::USER_CREDENTIALS_FIELD_NAMES['PASSWORD_HASH']]))
			{
				$userDetails = $dbHelper->getUserDetails($userName);
				
				$userActivationDetails = $dbHelper->getUserActivationDetails($userName);
				
				if($userActivationDetails[Literals::USER_ACCOUNT_STATUS_FIELD_NAMES['ACCOUNT_STATUS']]==0)
				{
					header("location:login.php?msg=4"); 
				}

				$userRecentSessionDetails = $dbHelper->getUserSessionDetails($userName);
				$recentSessionID = $userRecentSessionDetails[Literals::USER_SESSION_FIELD_NAMES['SESSION_ID']];
				$recentSessionIP = $userRecentSessionDetails[Literals::USER_SESSION_FIELD_NAMES['SESSION_IP']];

				if($recentSessionID == session_id() && $recentSessionIP == $ipAddr && isset($_SESSION['user_name']))
				{
					header("location:home.php");
				}

				$authCode = $dbHelper->createUserAuthCode($userName);
				
				$to = $userDetails[Literals::USER_DETAILS_FIELD_NAMES['EMAIL']];
				$subject = "Primechain Auth Code";
				$body = "Your OTP for login is" . "\r\n" . "<b>" . $authCode . "</b>" . "\r\n" ;
				$headers = "From: noreply@example.com" . "\r\n";
				$headers .= 'Reply-To: noreply@example.com' . "\r\n";
				$headers .= "Return-Path: noreply@example.com"."\r\n";
				$headers .= 'X-Mailer: PHP/' . phpversion() . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				
				mail($to,$subject,$body,$headers);

				$_SESSION['user_name'] = $userName;
				header("location:2stepauth.php");
			}
			else
			{
				header("location:login.php?msg=1");
			}

		}
		else
		{
			header("location:login.php?msg=1");
			ob_end_flush();
			exit();
		}
	}
	catch(Exception $ex)
	{
		echo "verify-login Exception: " . $ex->getMessage();
	}

	include ("bottom.php");
	ob_end_flush();
?>