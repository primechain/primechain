<?php
    session_start();
    ob_start();
    require_once('check-login.php');
	include_once 'top-logged-in.php';
    //include_once 'top.php';
?>

<script type="text/javascript" src="js/contract.js"></script>

<section role="main" class="content-body">

    <header class="page-header">
        <h2>PrimeContract</h2>
    
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="index.php">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>PrimeContract</span></li>
                <li><span>History</span></li>
            </ol>
    
            <a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a><!--  data-open="sidebar-right" -->
        </div>
    </header>

    <div class="row">
        <div class="col-md-12">
            <section class="panel panel-primary">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                        <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
                    </div>

                    <h2 class="panel-title">Contracts History</h2>
                </header>
                <div class="panel-body">
                    <!-- <form action="verify-login.php" method="post"> -->

                        <div class="row appear-animation fadeIn appear-animation-visible">
                            <div class="col-md-12">

                                <?php
                                    include_once 'dbhelper.php';
                                    include_once 'resources.php';

                                    $dbHelper = new DBHelper(session_id(), $_SERVER);
                                    $contractsDetails = $dbHelper->getContractsHistoryForUser($_SESSION['user_name']);

                                    if (count($contractsDetails)>0)
                                    {

                                        echo "<p class='appear-animation fadeInDown appear-animation-visible'><table class='table table-bordered table-hover'>";
                                        echo "<tr><th>".Literals::CONTRACT_DETAILS_FIELD_DESC['title']."</th><th>".Literals::CONTRACT_DETAILS_FIELD_DESC['dou']."</th><th>".Literals::CONTRACT_DETAILS_FIELD_DESC['desc']."</th><th></th><th></th></tr>";

                                        foreach ($contractsDetails as $contractDetails)
                                        {
                                            echo "<tr>";

                                            foreach ($contractDetails as $key => $value)
                                            {
                                                if ($key != Literals::CONTRACT_DETAILS_FIELD_NAMES['FILE_HASH'] && $key != Literals::CONTRACT_DETAILS_FIELD_NAMES['CONTRACT_ID']) {
                                                    echo "<td>".$value."</td>";
                                                }
                                            }

                                            echo "<td><a class='mb-xs mt-xs mr-xs btn btn-primary' target='_new' href='contract_upload_details.php?contractid=".$contractDetails[Literals::CONTRACT_DETAILS_FIELD_NAMES['CONTRACT_ID']]."'>View</a></td>";

                                            echo "<td><a class='mb-xs mt-xs mr-xs btn btn-success' target='_new' href='contract_invite.php?contractid=".$contractDetails[Literals::CONTRACT_DETAILS_FIELD_NAMES['CONTRACT_ID']]."'>Invite Signees</a></td>";
                                            
                                            echo "</tr>";
                                        }

                                        echo "</table></p>";
                                    }
                                ?>

                            </div>
                        </div>

                    <!-- </form> -->
                </div>
            </section>
        </div>

    </div>

</section>

<?php
    include_once 'bottom-logged-in.php';
?>