<?php

	include_once("dbhelper.php");
	include_once("resources.php");

	try
	{
		if(isset($_SESSION['loggedin'])) 
		{
			if($_SESSION['loggedin'] == false){ header("location:login.php?msg=2"); exit(); }
		}
		else
		{
			header("location:login.php?msg=2");
			//echo "logged in - ".$_SESSION['loggedin'];
		}

		if(isset($_SESSION['user_name']))
		{
			$userName = $_SESSION['user_name'];
			$dbHelper = new DBHelper(session_id(), $_SERVER);
			$userRecentSessionDetails = $dbHelper->getUserSessionDetails($userName);
			$recentSessionID = $userRecentSessionDetails[Literals::USER_SESSION_FIELD_NAMES['SESSION_ID']];
			$recentSessionIP = $userRecentSessionDetails[Literals::USER_SESSION_FIELD_NAMES['SESSION_IP']];

			if ($recentSessionIP!=$_SERVER['REMOTE_ADDR']) { header("location:login.php?msg=7"); }
			if ($recentSessionID!=session_id()) { header("location:login.php?msg=6"); }

		}
		else
		{
			header("location:login.php?msg=2");
		}

		$userName = isset($_SESSION['user_name'])?$_SESSION['user_name']:"";

	}
	catch(Exception $ex)
	{
		echo "Check-login Exception: " . $ex->getMessage();
	}
	
?>