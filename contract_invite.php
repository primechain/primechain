<?php
    session_start();
    ob_start();
    require_once('check-login.php');
	include_once 'top-logged-in.php';
    //include_once 'top.php';
?>

<script type="text/javascript" src="js/contract.js"></script>

<section role="main" class="content-body">

    <header class="page-header">
        <h2>PrimeContract</h2>
    
        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="index.php">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>PrimeContract</span></li>
                <li><span>Invite Signees</span></li>
            </ol>
    
            <a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a><!--  data-open="sidebar-right" -->
        </div>
    </header>

    <div class="row">
        <div class="col-md-12">
            <section class="panel panel-primary">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                        <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
                    </div>

                    <h2 class="panel-title">Invite Signees</h2>
                </header>
                <div class="panel-body">
                    <!-- <form action="verify-login.php" method="post"> -->

                    	<div class="form-group appear-animation fadeIn appear-animation-visible">
                            <label class="col-sm-3 control-label"><strong>Contract ID </strong></label>
                            <div class="col-sm-9">
                                <input id="contractid" name="contractid" class="form-control" placeholder="Contract ID" value="<?php echo (isset($_GET['contractid'])) ? $_GET['contractid'] : "" ?>" required="true">
                            </div>
                        </div>
                        <br/>

                        <div class="form-group appear-animation fadeIn appear-animation-visible">
                            <label class="col-sm-3 control-label"><strong>Enter usernames (seperated by commas)</strong></label>
                            <div class="col-sm-9">
                                <textarea id="invitees" name="invitees" rows="5" class="form-control" placeholder="Enter usernames (seperated by commas)" required></textarea>
                            </div>
                        </div>
                        <br/>

                        <div class="row appear-animation fadeIn appear-animation-visible">
                            <div class="col-sm-12 text-left">
                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary" onclick="inviteSignees(this, contractid, invitees, output);">Invite</button>
                                <!-- <button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Sign In</button> -->
                            </div>
                        </div><br/>

                        <div class="row appear-animation fadeIn appear-animation-visible">
                            <div id="output" class="col-md-11">
                            </div>
                        </div>


                    <!-- </form> -->
                </div>
            </section>
        </div>

    </div>

</section>

<?php
    include_once 'bottom-logged-in.php';
?>