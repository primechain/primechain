<?php
	session_start();
    ob_start();
    require_once('check-login.php');
	include_once 'top-logged-in.php';
	// include_once 'top.php';
?>

<script src="js/indiacoin.js" type="text/javascript"></script>

<script>

    function myTimer() {
        getNativeCurrencyBalance('txtMyAddress', 'pWalletBalance');
    }

    window.onload = function(){ myTimer(); setInterval(myTimer, 10000); };

</script>

<section role="main" class="content-body">

	<header class="page-header">
		<h2>Indiacoin</h2>
	
		<div class="right-wrapper pull-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.php">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><span>Send Money</span></li>
			</ol>
			
			<a class="sidebar-right-toggle"><i class="fa fa-chevron-left"></i></a><!--  data-open="sidebar-right" -->
		</div>
	</header>

	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-5">
			<div class="row">
				<label><strong>Your Wallet Address</strong></label>
			</div>
			<div class="row">
				<input id="txtMyAddress" type="text" class="form-control" size=45 value="<?php echo $_SESSION['address'] ?>" readonly="readonly" />
			</div>
		</div>
		<div class="col-md-1"></div>
		<div class="col-md-4">
			<strong>Balance</strong> - <b><label id="pWalletBalance"></label></b>
		</div>
	</div><br/>

	<div class="row">
		<div class="col-md-10">
            <section class="panel panel-tertiary">
                <header class="panel-heading">
                    <div class="panel-actions">
                        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                        <!-- <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a> -->
                    </div>

                    <h2 class="panel-title">Send Indiacoins with Message</h2>
                </header>
                <div class="panel-body">
                    <!-- <form action="verify-login.php" method="post"> -->

                        <div class="form-group appear-animation fadeIn appear-animation-visible">
                            <label class="col-sm-3 control-label"><strong>Send to </strong></label>
                            <div class="col-sm-9">
                                <input id="txtToAddrSWM" type="text" placeholder="" class="form-control" value="" size=50 />
                            </div>
                        </div>
                        <br/>

                        <div class="form-group appear-animation fadeIn appear-animation-visible">
                            <label class="col-sm-3 control-label"><strong>Message </strong></label>
                            <div class="col-sm-9">
                                <textarea id="txtMessageSWM" name="txtMessageSWM" rows="5" class="form-control" placeholder="Description" required></textarea>
                            </div>
                        </div>
                        <br/>

                        <div class="form-group appear-animation fadeIn appear-animation-visible">
                            <label class="col-sm-3 control-label"><strong>Indiacoins </strong></label>
                            <div class="col-sm-9">
                                <input id="txtUnitsSWM" type="number" step="any" placeholder="" class="form-control" value="1" width=30 />
                            </div>
                        </div>
                        <br/><br/>

                        <div class="form-group appear-animation fadeIn appear-animation-visible">
                            <div class="col-sm-9">
                                <button type="button" class="mb-xs mt-xs mr-xs btn btn-primary" onclick="sendMetadataToAddress('txtMyAddress', 'txtToAddrSWM', 'txtMessageSWM', 'txtUnitsSWM', this, 'outputSWM');">Send</button>
                                <!-- <button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Sign In</button> -->
                            </div>
                        </div><br/>

                        <div id="outputSWM" class="appear-animation bounceIn appear-animation-visible">

                        </div>



                    <!-- </form> -->
                </div>
            </section>
        </div>
		
	</div>

</section>

<?php
	include_once 'bottom-logged-in.php';
	// include_once 'bottom.php';
	ob_end_flush();
?>