<?php
	ob_start();
	include_once ("top.php");
	require_once("helperFunctions.php");
	require_once("dbhelper.php");
	include_once "config.php";
?>

<section role="main" class="content-body">

	<header class="page-header">
		<h2>Dashboard</h2>
	
		<div class="right-wrapper pull-right">
			<ol class="breadcrumbs">
				<li>
					<a href="index.php">
						<i class="fa fa-home"></i>
					</a>
				</li>
				<li><span>Dashboard</span></li>
			</ol>
	
			<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
		</div>
	</header>

	<div class="row">
		<div class="col-md-10">
<?php

	try
	{
		
		if(isset($_POST['username']))
		{
			$name = htmlspecialchars($_POST['name'], ENT_QUOTES, 'UTF-8');
			$userName = htmlspecialchars($_POST['username'], ENT_QUOTES, 'UTF-8');
			$email = $_POST['email'];
			$org = htmlspecialchars($_POST['org'], ENT_QUOTES, 'UTF-8');
			$country = htmlspecialchars($_POST['country'], ENT_QUOTES, 'UTF-8');

			if(!validateEmail($email))
			{
				header("location:register.php?msg=1");
				exit();
			}

/*			if(!validateName($name))
			{
				header("location:register.php?msg=6");
				exit();
			}*/

/*			if(!validateUsername($userName))
			{
				header("location:register.php?msg=3");
				exit();
			}

			if(!validatePassword($password))
			{
				header("location:register.php?msg=4");
				exit();
			}
*/
			/*if($password != $confirmPassword)
			{
				header("location:register.php?msg=2");
				exit();
			}*/

			$dbHelper = new DBHelper(session_id(), $_SERVER);
			
			if ($dbHelper->userExists($userName)) {
				header("location:register.php?msg=5");
				exit;				
			}

			//print_r($dbh);
			//echo "<br/>";
			//$activationCode = strval(randomNDigitNumber(8));
			$password = generatePassword();

			$activationCode = $dbHelper->createUser($userName, $password, $name, $email, $org, $country);
			
			if ($activationCode===false) {
				throw new Exception("Registration failed!!!", 1);				
			}
			$to = $email;
			$activationURL = "http://".WebServerParams::HOST_NAME."/".WebServerParams::PRIMECHAIN_ROOT_DIR."/"."activation.php?username=".$userName."&activationcode=".$activationCode;

			$subject = "Activate your Primechain account";

			$body = "Your user name is <b>".$userName."</b><br/>Your password is <b>".$password."</b><br/>Click <a href='".$activationURL."' target='_new'>here</a> or visit the following link to activate your account.<br/>".$activationURL."\r\n";
			$headers = "From: noreply@example.com" . "\r\n";
			$headers .= 'Reply-To: noreply@example.com' . "\r\n";
			$headers .= "Return-Path: noreply@example.com"."\r\n";
			$headers .= 'X-Mailer: PHP/' . phpversion() . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			mail($to,$subject,$body,$headers);


			echo "<p class='lead'><b><font color='blue'>Account registration successful. Please click the activation link sent to your registered email id to activate your account.</font></b></p>";
		}
		else if(!isset($_POST['username']))
		{
			header("location:register.php?msg=3");
		}
		else if(!isset($_POST['password']))
		{
			header("location:register.php?msg=4");
		}
		else
		{
			header("location:register.php?msg=8");
		}

		
	}
	catch(Exception $ex)
	{
		echo $ex->getMessage();
		//header("location:register.php?msg=8");
	}

?>

		</div>
	</div>
</section>

<?php
	ob_end_flush();
	include ("bottom.php");
?>