<?php
    session_start();
    ob_start();
    require_once('check-login.php');
    ini_set('error_reporting', E_ALL);
    error_reporting(E_ALL);

    try
    {
        include_once 'check-login.php';
        include_once 'helperFunctions.php';
        include_once 'config.php';
        include_once 'resources.php';
        include_once 'dbhelper.php';
        require_once('MultichainClientTest.php');

        date_default_timezone_set("Asia/Kolkata");

        if(isset($_POST['fromaddr'])){
            if($_POST['fromaddr'] != $_SESSION['address']){ 
                throw new Exception("This address (".$_POST['fromaddr'].") does not belong to you."); 
            }
            $userAddress = $_POST['fromaddr']; 
        } 
        else if(isset($_SESSION['address'])){ 
            $userAddress = $_SESSION['address'];
        } 
        else {
            $userAddress = "";
        };

        $toAddress = isset($_POST['toaddr'])?$_POST['toaddr']:"";
        $cmd = isset($_POST['cmd'])?$_POST['cmd']:"";
        $units = isset($_POST['units'])?floatval($_POST['units']):floatval(0);

        if (floatval($units)<0) { throw new Exception("Units can't be negative"); }

        $metadata = isset($_POST['metadata']) ? htmlspecialchars($_POST['metadata'], ENT_QUOTES) : "";
        //$MCTest = unserialize($_SESSION['MCTest']);
        $MCTest = new MultichainClientTest();
        $MCTest->setUp(MultichainParams::HOST_NAME, MultichainParams::RPC_PORT, MultichainParams::RPC_USER, MultichainParams::RPC_PASSWORD);

        $validateFromAddressResponse = $MCTest->testValidateAddress($userAddress);

        if(!$validateFromAddressResponse['isvalid'])
        {
            header("location:login.php?msg=1");
        }

        if($cmd == Literals::MULTICHAIN_COMMANDS_CODES['NCB'])
        {
                $nativeCurrencyBalance = $MCTest->testGetAssetBalanceForAddressByAssetRef($userAddress, IndiacoinParams::ASSET_REF);
                echo "<strong><font color='blue'>".strval(number_format($nativeCurrencyBalance, 2))."</font></strong>";
        }
        else if($cmd == Literals::MULTICHAIN_COMMANDS_CODES['MARK_READ'])
        {
            /*
            $STM = $dbh->prepare("UPDATE indiacoin_messages SET is_read=true WHERE to_address=:to_address");
            $STM->bindParam(':to_address', $userAddress);
            $STM->execute();*/
        }
        else if($cmd == Literals::MULTICHAIN_COMMANDS_CODES['FETCH_MSGS'])
        {
            /*
            $STM = $dbh->prepare("SELECT A.msg_id, A.from_address, A.to_address, A.is_read, B.username FROM indiacoin_messages A JOIN indiacoin_addresses B ON B.address = A.from_address WHERE A.to_address=:to_address AND A.is_read=:is_read");
            $STM->bindParam(':to_address', $userAddress);
            $bool_false = false;
            $STM->bindParam(':is_read', $bool_false);
            $STM->execute();
            $msgRows = $STM->fetchAll();
            $rowCount = $STM->rowCount();

            if ($rowCount > 0) {

                $response = "<table class='tblchat'><tbody class='tblchat'>";
                
                foreach ($msgRows as $msgRow) {
                    //print_r($msgRow);
                    $transaction = $MCTest->testGetAddressTransaction($userAddress, $msgRow['msg_id']);
                    $data = hex2bin($transaction['data'][0]);
                    $assetsArr = $transaction['balance']['assets'];
                    $response .= "<tr><td>"."<strong>".$msgRow['username']."</strong>"."</td><td>&nbsp;<i>(".date('m-d-Y'.',  '.'h:i:s a', $transaction['time']).")</i>:"."</td></tr>";

                    if (count($assetsArr)>0)
                    {
                        $key = array_search(IndiacoinParams::ASSET_REF, array_column($assetsArr, 'assetref'));
                        if (!($key===false)) {
                            $response .= "<tr><td colspan=2>"."<i>Amount transferred</i> - ".strval(number_format($assetsArr[$key]['qty'], 2))." Indiacoins"."<br/>"."<i>Message</i> - ".$data."</td></tr>";
                        }
                    }
                    else
                    {
                        $response .= "<tr><td colspan=2>"."<i>Message</i> - ".$data."<br/></td></tr>";
                    }
                }

                $response .= "</tbody></table>";
                echo $response;
            }
            else
            {
                echo "No unread message(s)";
            }
            */
        }
        else if($cmd == Literals::MULTICHAIN_COMMANDS_CODES['FETCH_MSGS_COUNT'])
        {
            /*
            $STM = $dbh->prepare("SELECT count(*) FROM indiacoin_messages WHERE to_address=:to_address AND is_read=:is_read");
            $STM->bindParam(':to_address', $userAddress);
            $bool_false = false;
            $STM->bindParam(':is_read', $bool_false);
            $STM->execute();
            $row = $STM->fetch();
            echo $row[0];*/
        }
        else if($cmd == Literals::MULTICHAIN_COMMANDS_CODES['GET_TX_DETAILS'])
        {
            $txId = isset($_POST['txid']) ? $_POST['txid'] : "";
            $amount = isset($_POST['amt']) ? $_POST['amt'] : 0;
            $transaction = $MCTest->testGetAddressTransaction($userAddress, $txId);

            echo "<h3>Transaction details</h3>";
            echo printTransactionBasicDetailsVertically($transaction, $userAddress, IndiacoinParams::ASSET_REF);

            if (isset($transaction['blockhash']) && $transaction['blockhash'] != "")
            {
                $blockDetails = $MCTest->testGetBlock($transaction['blockhash']);

                if (is_array($blockDetails))
                {
                    echo "<br/><br/>";
                    echo "<h3>Block details</h3>";
                    echo printBlockDetailsVertically($blockDetails);
                }
                
            }
        }
        else if($cmd == Literals::MULTICHAIN_COMMANDS_CODES['GET_TRANSACTIONS_HISTORY'])
        {
            $transactions = $MCTest->testListAddressTransactions($userAddress, 30);
            $transactions = array_reverse($transactions);
            // echo "<h3>Transactions History</h3>";
            echo printTransactionsHistory($transactions, $userAddress, IndiacoinParams::ASSET_REF);
        }
        else
        {
            $dbHelper = new DBHelper(session_id(),$_SERVER);

            if (!$dbHelper->isAddressValid($toAddress)) {
                $toAddress = $dbHelper->getUserAddress($toAddress);
            }

            //$toAddress = getAddressForUserNameOrAddress($toAddress);

            if($cmd == Literals::MULTICHAIN_COMMANDS_CODES['STA'])
            {
                $txId = $MCTest->testSendAssetFrom($userAddress, $toAddress, IndiacoinParams::ASSET_REF, $units);
                // echo "<strong><font color='green'>"."Transaction successful.</font> <br/><font color='green'>Transaction ID :</font><br/>"."<a id='btnTxId' style='font-size:13px' data-toggle='modal' data-target='#transactionDetailsModal' onclick='fetchTransactionDetails(\"$txId\", txtUnitsSTA.value, \"txdetailsoutput\")'>$txId</a>"."<br/><font color='green'>".strval(number_format($units, 2))."</font> Indiacoins transferred"."</strong>";

                echo "<strong><font color='green'>"."Transaction successful.</font> <br/><font color='green'>Transaction ID :</font><br/>"."<a href='".ExplorerParams::TX_URL_PREFIX.$txId."' target='_new'>$txId</a>"."<br/><font color='green'>".strval(number_format($units, 2))."</font> Indiacoins transferred"."</strong>";
            }
			else if($cmd == Literals::MULTICHAIN_COMMANDS_CODES['SWM'])
            {
                if ($units<=0.0) {
                    throw new Exception("Amount cannot be zero or negative.", 1);                        
                }
                
                //$toAddress = getAddressForUserNameOrAddress($toAddress);

                

                $txId = $MCTest->testSendWithMetadataFrom(bin2hex($metadata), $userAddress, $toAddress, $units, IndiacoinParams::ASSET_REF);
                
                /*
                $STM = $dbh->prepare("INSERT INTO indiacoin_messages (msg_id, from_address, to_address) VALUES (:msg_id, :from_address, :to_address)");
                $STM->bindParam(':msg_id', $txId);
                $STM->bindParam(':from_address', $userAddress);
                $STM->bindParam(':to_address', $toAddress);
                $STM->execute();
                */

                // echo "<strong><font color='green'>"."Transaction successful.</font> <br/><font color='green'>Transaction ID :</font><br/>"."<a id='btnTxId' style='font-size:13px' data-toggle='modal' data-target='#transactionDetailsModal' onclick='fetchTransactionDetails(\"$txId\", txtUnitsSWM.value, \"txdetailsoutput\")'>$txId</a>"."<br/><font color='green'>Message Sent : </font>".$metadata."<br/><font color='green'>".strval(number_format($units, 2))."</font> Indiacoins transferred"."</strong>";

                echo "<strong><font color='green'>"."Transaction successful.</font> <br/><font color='green'>Transaction ID :</font><br/>"."<a href='".ExplorerParams::TX_URL_PREFIX.$txId."' target='_new'>$txId</a>"."<br/><font color='green'>Message Sent : </font>".$metadata."<br/><font color='green'>".strval(number_format($units, 2))."</font> Indiacoins transferred"."</strong>";
            }
            else 
            {
                throw new Exception("Error Processing Request", 1);                    
            }
        }

        //$_SESSION['MCTest'] = serialize($MCTest);
    }
    catch(Exception $ex)
    {
        //throw $ex;        
        echo "<h4><font color='red'>Error: ".$ex->getMessage()."</font></h4>";
    }
	
	ob_end_flush();
?>